# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2022/02/15 17:52:28 by davifah          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

COMMON_FILES = parse_args parse_utils operations/push operations/swap \
			  operations/rev_rotate operations/rotate sort_utils error \
			  group_prepare group_utils grouped_utils

PS_FILES	= ${COMMON_FILES} push_swap grouped_sort catchup special_cases \
			  push_back

BONUS_FILES	= ${COMMON_FILES} checker operations/check_op checker_utils

PS_TEMP		= $(addprefix src/, ${PS_FILES})
PS			= $(addsuffix .c, ${PS_TEMP})
PS_OBJS		= ${PS:.c=.o}

SRCS_TEMP	= $(addprefix src/, ${BONUS_FILES})
SRCS		= $(addsuffix .c, ${SRCS_TEMP})
BONUS_OBJS	= ${SRCS:.c=.o}

NAME		= push_swap

CC			= gcc -Wall -Wextra -Werror

INCLUDES	= -I./includes -I./libft/includes

LIB			= -L./libft/ -lft
LIBFT		= libft/libft.a
LIBFT_ARGS	=

SYSTEM		= $(shell uname -s)

ifdef DEFINE
$(shell rm -f ${NAME} src/group_prepare.o)
INCLUDES	:= ${INCLUDES} -D GP_SIZE=${DEFINE}
endif

${NAME}:	${LIBFT} ${PS_OBJS}
			${CC} ${INCLUDES} -o ${NAME} ${PS_OBJS} ${LIB}

.c.o:
			${CC} -c ${INCLUDES} $< -o ${<:.c=.o} ${OPT_DEF}


bonus:		checker

checker:	${LIBFT} ${BONUS_OBJS}
			${CC} ${INCLUDES} -o checker ${BONUS_OBJS} ${LIB}

all:		${NAME}

# cmd to prof code:
# gprof ${NAME} gmon.out > analysis.txt
profile:	fclean
profile:	CC := ${CC} -pg
profile:	LIBFT_ARGS := ${LIBFT_ARGS} PROFILE=1
profile:	${NAME}

${LIBFT}:
			$(MAKE) -C ./libft expanded ${LIBFT_ARGS}
			$(MAKE) -C ./libft get-next-line ${LIBFT_ARGS}

git:
			git submodule update --init --recursive

ifeq (${FILE}, mine)
CHECKER		:= ./checker
else ifeq (${SYSTEM}, Linux)
CHECKER		:= ./tests/checker_linux
else ifeq (${SYSTEM}, Darwin)
CHECKER		:= ./tests/checker_Mac
endif

moves:		${NAME}
			@echo Amount of moves: $$(./${NAME} ${ARG} | wc -l | xargs)

check:		moves ${NAME}
			@chmod +x ${CHECKER}
			./${NAME} ${ARG} | ${CHECKER} ${ARG}

pyviz:		${NAME}
			@cp tests/pyviz.py .
			python3 pyviz.py ${ARG}
			@rm -f pyviz.py

clean:
			rm -f ${PS_OBJS} ${BONUS_OBJS}
			make -C ./libft clean

fclean:		clean
			rm -f ${NAME} checker libft/libft.a

re:			fclean all

.PHONY:		all clean fclean re bonus git moves check
