/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grouped_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 21:44:08 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 17:47:55 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"
#include "sort.h"
#include "grouped_sort.h"
#include "push_swap.h"

static void	push_group(int gpn, t_group *tg);

void	grouped_sort(t_group *tg)
{
	int	i;
	int	gp_start;

	gp_start = tg->groupn / 2;
	if (tg->len <= 0)
		(void)tg->len;
	else
	{
		i = -1;
		while (gp_start + ++i < tg->groupn)
		{
			push_group(gp_start + i, tg);
		}
		i = -1;
		while (gp_start - ++i >= 0)
			catchup(gp_start - i, tg);
	}
	while (stack_size(tg->a))
	{
		pb(tg->a, tg->b, tg->len);
	}
	push_back(tg);
}

static void	push_gplower(t_group *tg, int gplower, int *pos)
{
	if (gplower >= 0 && tg->group_nleft[gplower]
		&& is_ingroup(**(tg->a), gplower, tg))
	{
		pb(tg->a, tg->b, tg->len);
		tg->group_nleft[gplower]--;
		if (*pos > 0)
		{
			rr(tg->a, tg->b);
			*pos = 0;
		}
		else
			rb(tg->b);
	}
}

static void	push_group(int gpn, t_group *tg)
{
	int	pos;
	int	gplower;

	while (tg->group_nleft[gpn])
	{
		gplower = gpn - 1;
		pos = next_push_pos(tg->a, tg, gpn);
		while (pos)
		{
			while (!tg->group_nleft[gplower] && gplower)
				gplower--;
			push_gplower(tg, gplower, &pos);
			if (pos > 0)
				ra(tg->a);
			else if (pos < 0)
				rra(tg->a);
			pos = next_push_pos(tg->a, tg, gpn);
		}
		pb(tg->a, tg->b, tg->len);
		tg->group_nleft[gpn]--;
		if (tg->group_nleft[gplower] < tg->group_size / 8)
			catchup(gplower, tg);
	}
	if (tg->group_nleft[gplower] < tg->group_size / 4)
		catchup(gplower, tg);
}
