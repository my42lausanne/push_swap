/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_cases.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/14 18:04:14 by davifah           #+#    #+#             */
/*   Updated: 2022/02/15 17:48:31 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"
#include "sort.h"
#include "push_swap.h"
#include "grouped_sort.h"

static void	sort_small_stacks(t_group *tg)
{
	int	b_size;

	b_size = stack_size(tg->a) - 3 > 1;
	while (stack_size(tg->a) > 3)
		pb(tg->a, tg->b, tg->len);
	if (b_size && **(tg->a) == stack_max(tg->a)
		&& **(tg->b) == stack_max(tg->b))
		rr(tg->a, tg->b);
	else if (**(tg->a) == stack_max(tg->a))
		ra(tg->a);
	else if (b_size && **(tg->b) == stack_max(tg->b))
		rb(tg->b);
	if (stack_size(tg->a) > 2 && *(tg->a[1]) == stack_max(tg->a)
		&& stack_size(tg->b) > 2 && *(tg->b[1]) == stack_max(tg->b))
		rrr(tg->a, tg->b);
	else if (stack_size(tg->a) > 2 && *(tg->a[1]) == stack_max(tg->a))
		rra(tg->a);
	else if (stack_size(tg->b) > 2 && *(tg->b[1]) == stack_max(tg->b))
		rrb(tg->b);
	if (b_size && **(tg->a) > *(tg->a[1]) && **(tg->b) > *(tg->b[1]))
		ss(tg->a, tg->b);
	else if (**(tg->a) > *(tg->a[1]))
		sa(tg->a);
	else if (b_size && **(tg->b) > *(tg->b[1]))
		sb(tg->b);
}

static void	goto_smallest(t_group *tg)
{
	int	pos;

	pos = get_position(tg->a, stack_min(tg->a));
	while (pos)
	{
		if (pos > 0)
			ra(tg->a);
		else
			rra(tg->a);
		pos = get_position(tg->a, stack_min(tg->a));
	}
}

static void	merge_small_stacks(t_group *tg)
{
	int	pos;

	pos = 0;
	while (stack_size(tg->b))
	{
		if (**(tg->b) < **(tg->a))
			pa(tg->b, tg->a, tg->len);
		else if (**(tg->b) > stack_max(tg->a))
		{
			while (pos--)
				rra(tg->a);
			pos = 0;
			pa(tg->b, tg->a, tg->len);
			ra(tg->a);
		}
		else
		{
			ra(tg->a);
			pos++;
		}
	}
	goto_smallest(tg);
}

void	sort_small(t_group *tg)
{
	sort_small_stacks(tg);
	merge_small_stacks(tg);
}
