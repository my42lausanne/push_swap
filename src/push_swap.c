/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 01:58:35 by davifah           #+#    #+#             */
/*   Updated: 2022/02/14 19:02:52 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "sort.h"
#include "grouped_sort.h"

int	main(int ac, char **av)
{
	int	**a_stack;
	int	len;

	a_stack = parse_args(ac, av, &len);
	sort(a_stack, len);
}

void	sort(int **a, int len)
{
	int		**b;
	t_group	*tg;

	b = ft_calloc(len + 1, sizeof(int *));
	if (!b)
		ft_error();
	tg = group_prepare(a, len);
	tg->a = a;
	tg->b = b;
	if (len <= 6 && !a_iscomplete(a, len))
		sort_small(tg);
	else
		while (!a_iscomplete(a, len))
			grouped_sort(tg);
	free_t_group(tg);
}
