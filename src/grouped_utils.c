/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grouped_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 21:50:26 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 15:17:47 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "grouped_sort.h"

int	next_push_pos(int **a, t_group *tg, int gpn)
{
	int	i;

	i = -1;
	while (1)
	{
		if (is_ingroup(*a[++i], gpn, tg))
			return (i);
		if (is_ingroup(negative_index(a, i), gpn, tg))
			return (-i);
	}
}

void	goto_position(int **a, int pos, char stack)
{
	while (pos)
	{
		if (pos > 0)
		{
			if (stack == 'a')
				ra(a);
			else
				rb(a);
			pos--;
		}
		if (pos < 0)
		{
			if (stack == 'a')
				rra(a);
			else
				rrb(a);
			pos++;
		}
	}
}

int	get_position(int **a, int n)
{
	int	i;

	i = -1;
	while (1)
	{
		if (*a[++i] == n)
			return (i);
		if (negative_index(a, i) == n)
			return (-i);
	}
}

/*
int	count_lower_nbrs(int **a, int n)
{
	int	c;
	int	i;

	i = -1;
	c = 0;
	while (a[++i])
	{
		if (*a[i] < n)
			c++;
	}
	return (c);
}
*/

int	is_ingroup(int n, int gp_n, t_group *tg)
{
	return (tg->group_max[gp_n] >= n && n >= tg->group_min[gp_n]);
}

int	negative_index(int **a, unsigned int index)
{
	int	i;

	i = 0;
	if (!index)
		return (*a[0]);
	while (a[i])
		i++;
	return (*a[i - index]);
}
