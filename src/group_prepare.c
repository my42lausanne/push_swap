/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   group_prepare.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/19 16:39:50 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 17:48:12 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "sort.h"
#include "grouped_sort.h"

#ifndef GP_SIZE
# define GP_SIZE 0
#endif

t_group	*group_prepare(int **a, int len)
{
	t_group	*tg;
	int		g_size;
	int		i;
	int		j;

	if (GP_SIZE)
		g_size = GP_SIZE;
	else if (len >= 500)
		g_size = 30;
	else if (len >= 100)
		g_size = 15;
	else
		g_size = 2;
	tg = group_create(a, len, g_size);
	i = -1;
	while (++i < tg->groupn)
	{
		tg->group_min[i] = tg->sorted_list[i * g_size];
		j = i * g_size;
		while (j + 1 < tg->len && j < (i + 1) * g_size - 1)
			j++;
		tg->group_max[i] = tg->sorted_list[j];
		tg->group_nleft[i] = j - (i * g_size) + 1;
	}
	return (tg);
}

void	free_t_group(t_group *tg)
{
	free_intarray(tg->a);
	free(tg->b);
	free(tg->sorted_list);
	free(tg->group_max);
	free(tg->group_min);
	free(tg->group_nleft);
	free(tg);
}
