/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 12:00:40 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/17 12:04:40 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "operations.h"

int	op_compare(char *a, char *b)
{
	return (ft_strlen(a) == ft_strlen(b)
		&& !ft_strncmp(a, b, 5));
}
