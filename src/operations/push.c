/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 17:03:33 by davifah           #+#    #+#             */
/*   Updated: 2021/12/17 10:59:19 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

int	push(int **src, int **dst, int len)
{
	int	i;

	if (!*src)
		return (1);
	i = len;
	while (--i >= 0)
	{
		if (!i)
			*dst = *src;
		else
		{
			dst[i] = dst[i - 1];
		}
	}
	while (++i < len)
	{
		src[i] = src[i + 1];
	}
	return (0);
}

void	pa(int **b, int **a, int len)
{
	if (!push(b, a, len))
		ft_putendl_fd("pa", 1);
}

void	pb(int **a, int **b, int len)
{
	if (!push(a, b, len))
		ft_putendl_fd("pb", 1);
}
