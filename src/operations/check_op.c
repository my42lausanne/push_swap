/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 12:11:01 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/17 12:56:37 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"
#include "checker.h"

void	check_ss(int **a, int **b)
{
	swap(a);
	swap(b);
}

void	check_rr(int **a, int **b)
{
	rotate(a);
	rotate(b);
}

void	check_rrr(int **a, int **b)
{
	rev_rotate(a);
	rev_rotate(b);
}
