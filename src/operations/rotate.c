/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 18:15:24 by davifah           #+#    #+#             */
/*   Updated: 2021/12/22 11:15:15 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

void	swap_intp(int **a, int **b);

int	used_len(int **array)
{
	int	i;

	i = 0;
	while (array[i])
		i++;
	return (i);
}

int	rotate(int **array)
{
	int	i;
	int	len;
	int	*temp;

	len = used_len(array);
	i = len;
	if (len <= 1)
		return (1);
	while (--i >= 0)
	{
		if (i == len - 1)
		{
			temp = array[i];
			array[i] = *array;
		}
		else
			swap_intp(&temp, &array[i]);
	}
	return (0);
}

void	rr(int **a, int **b)
{
	int	res;

	res = 0;
	res += rotate(a);
	res += rotate(b);
	if (res != 2)
		ft_putendl_fd("rr", 1);
}

void	ra(int **a)
{
	if (!rotate(a))
		ft_putendl_fd("ra", 1);
}

void	rb(int **b)
{
	if (!rotate(b))
		ft_putendl_fd("rb", 1);
}
