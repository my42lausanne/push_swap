/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 16:43:50 by davifah           #+#    #+#             */
/*   Updated: 2021/12/17 10:58:34 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

int	swap(int **array)
{
	int	c;

	if (!array[0] || !array[1])
		return (1);
	c = *array[0];
	*array[0] = *array[1];
	*array[1] = c;
	return (0);
}

void	ss(int **a, int **b)
{
	int	res;

	res = 0;
	res += swap(a);
	res += swap(b);
	if (!res)
		ft_putendl_fd("ss", 1);
}

void	sa(int **a)
{
	if (!swap(a))
		ft_putendl_fd("sa", 1);
}

void	sb(int **b)
{
	if (!swap(b))
		ft_putendl_fd("sb", 1);
}
