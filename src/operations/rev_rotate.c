/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_rotate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 18:15:24 by davifah           #+#    #+#             */
/*   Updated: 2021/12/22 11:15:40 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

int	used_len(int **array);

void	swap_intp(int **a, int **b)
{
	int	*c;

	c = *a;
	*a = *b;
	*b = c;
}

int	rev_rotate(int **array)
{
	int	i;
	int	len;
	int	*temp;

	len = used_len(array);
	i = -1;
	if (len <= 1)
		return (1);
	while (++i < len)
	{
		if (i == 0)
		{
			temp = array[i];
			array[i] = array[len - 1];
		}
		else
			swap_intp(&temp, &array[i]);
	}
	return (0);
}

void	rrr(int **a, int **b)
{
	int	res;

	res = 0;
	res += rev_rotate(a);
	res += rev_rotate(b);
	if (res != 2)
		ft_putendl_fd("rrr", 1);
}

void	rra(int **a)
{
	if (!rev_rotate(a))
		ft_putendl_fd("rra", 1);
}

void	rrb(int **b)
{
	if (!rev_rotate(b))
		ft_putendl_fd("rrb", 1);
}
