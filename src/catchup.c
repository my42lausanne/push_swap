/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   catchup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/14 14:38:19 by davifah           #+#    #+#             */
/*   Updated: 2022/02/15 17:47:46 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"
#include "sort.h"
#include "grouped_sort.h"
#include "push_swap.h"

static void	push_gpn(t_group *tg, int gpn, int *pos)
{
	if (gpn < tg->groupn && tg->group_nleft[gpn]
		&& is_ingroup(**(tg->a), gpn, tg))
	{
		pb(tg->a, tg->b, tg->len);
		tg->group_nleft[gpn]--;
		if (*pos > 0)
			ra(tg->a);
		else
			rra(tg->a);
	}
}

void	catchup(int gplower, t_group *tg)
{
	int	pos;
	int	gpn;

	while (tg->group_nleft[gplower])
	{
		pos = next_push_pos(tg->a, tg, gplower);
		while (pos)
		{
			gpn = gplower + 1;
			while (!tg->group_nleft[gpn] && tg->groupn > gpn + 1)
				gpn++;
			push_gpn(tg, gpn, &pos);
			if (pos > 0)
				ra(tg->a);
			else if (pos < 0)
				rra(tg->a);
			pos = next_push_pos(tg->a, tg, gplower);
		}
		pb(tg->a, tg->b, tg->len);
		rb(tg->b);
		tg->group_nleft[gplower]--;
	}
}
