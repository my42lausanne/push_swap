/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 11:17:45 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/17 17:03:23 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "sort.h"
#include "get_next_line.h"

static int	check(int **a, int len);
static void	check_operation(char *op, int **a, int **b, int len);
int			op_compare(char *a, char *b);

int	main(int ac, char **av)
{
	int	**a_stack;
	int	len;

	a_stack = parse_args(ac, av, &len);
	if (len)
	{
		if (check(a_stack, len))
			ft_putstr_fd("OK\n", 1);
		else
			ft_putstr_fd("KO\n", 1);
	}
	free_intarray(a_stack);
}

static int	is_allwhite(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!ft_isspace(str[i]))
			return (0);
	return (1);
}

static int	check(int **a, int len)
{
	int		is_done;
	int		**b;
	char	*op;

	b = ft_calloc(len + 1, sizeof(int *));
	if (!b)
		ft_error();
	is_done = 0;
	while (!is_done)
	{
		op = get_next_line(0);
		if (!op)
			is_done = 1;
		else if (is_allwhite(op))
			continue ;
		else
		{
			check_operation(op, a, b, len);
		}
	}
	free(b);
	return (a_iscomplete(a, len));
}

static void	check_operation(char *op, int **a, int **b, int len)
{
	if (op_compare(op, "sa\n"))
		swap(a);
	else if (op_compare(op, "sb\n"))
		swap(b);
	else if (op_compare(op, "ss\n"))
		check_ss(a, b);
	else if (op_compare(op, "pa\n"))
		push(b, a, len);
	else if (op_compare(op, "pb\n"))
		push(a, b, len);
	else if (op_compare(op, "ra\n"))
		rotate(a);
	else if (op_compare(op, "rb\n"))
		rotate(b);
	else if (op_compare(op, "rr\n"))
		check_rr(a, b);
	else if (op_compare(op, "rra\n"))
		rev_rotate(a);
	else if (op_compare(op, "rrb\n"))
		rev_rotate(b);
	else if (op_compare(op, "rrr\n"))
		check_rrr(a, b);
	else
		ft_error();
}
