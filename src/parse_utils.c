/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 14:28:28 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 17:51:01 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	word_len(char *str)
{
	int	i;
	int	c;

	i = -1;
	c = 0;
	while (!ft_isspace(str[++i]) && str[i])
		c++;
	return (c);
}

int	set_level(char *word)
{
	int	len;

	len = ft_strlen(word);
	len--;
	if (word[0] == '-' || word[0] == '+')
		len--;
	return (ft_power(10, len));
}

void	print_arrays(int **a, int **b, int len)
{
	int	i;

	return ;
	i = -1;
	while (++i < len)
	{
		if (a[i])
			ft_putnbr_fd(*a[i], 1);
		else
			ft_putstr_fd("(0)", 1);
		ft_putstr_fd("\t", 1);
		if (b[i])
			ft_putnbr_fd(*b[i], 1);
		else
			ft_putstr_fd("(0)", 1);
		ft_putstr_fd("\n", 1);
	}
}

void	print_intarray(int **array, int len)
{
	int	i;

	i = -1;
	while (++i < len)
	{
		if (!array[i])
			ft_putstr_fd("(null)", 1);
		else
			ft_putnbr_fd(*array[i], 1);
		ft_putendl_fd("", 1);
	}
}

void	free_intarray(int **array)
{
	int	i;

	i = -1;
	while (array[++i])
		free(array[i]);
	free(array);
}
