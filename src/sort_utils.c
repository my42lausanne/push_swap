/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 12:28:08 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/17 22:56:28 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "sort.h"
#include "operations.h"
#include <limits.h>

int	stack_min(int **array)
{
	int	min;
	int	i;

	min = INT_MAX;
	i = -1;
	while (array[++i])
		if (*array[i] < min)
			min = *array[i];
	return (min);
}

int	stack_max(int **array)
{
	int	max;
	int	i;

	max = INT_MIN;
	i = -1;
	while (array[++i])
		if (*array[i] > max)
			max = *array[i];
	return (max);
}

int	stack_size(int **array)
{
	int	i;

	i = 0;
	while (array[i])
		i++;
	return (i);
}

int	stack_issorted(int **array)
{
	int	i;

	i = -1;
	while (array[++i])
		if (i && (*array[i - 1] > *array[i]))
			return (0);
	return (1);
}

int	a_iscomplete(int **a, int len)
{
	int	i;

	if (!stack_issorted(a))
		return (0);
	i = 0;
	while (a[i])
		i++;
	return (len == i);
}
