/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   group_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/19 17:10:33 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 17:48:03 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "grouped_sort.h"
#include "push_swap.h"

static void	sort_list(int *array, int len)
{
	int	changed;
	int	i;
	int	t;

	changed = 1;
	while (changed)
	{
		changed = 0;
		i = 0;
		while (++i < len)
		{
			if (array[i - 1] > array[i])
			{
				changed++;
				t = array[i - 1];
				array[i - 1] = array[i];
				array[i] = t;
			}
		}
	}
}

t_group	*group_create(int **a, int len, int group_size)
{
	int		i;
	t_group	*tg;

	tg = ft_calloc(1, sizeof(t_group));
	if (!tg)
		ft_error();
	tg->groupn = len / group_size;
	if (len % group_size)
		tg->groupn++;
	tg->group_size = group_size;
	tg->sorted_list = ft_calloc(len, sizeof(int));
	tg->group_max = ft_calloc(tg->groupn, sizeof(int));
	tg->group_min = ft_calloc(tg->groupn, sizeof(int));
	tg->group_nleft = ft_calloc(tg->groupn, sizeof(int));
	if (!tg->sorted_list || !tg->group_max || !tg->group_min
		|| !tg->group_nleft)
		ft_error();
	i = -1;
	while (++i < len)
		tg->sorted_list[i] = *a[i];
	sort_list(tg->sorted_list, len);
	tg->len = len;
	return (tg);
}
