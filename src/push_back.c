/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_back.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/15 14:29:29 by davifah           #+#    #+#             */
/*   Updated: 2022/02/15 17:48:19 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"
#include "sort.h"
#include "grouped_sort.h"
#include "push_swap.h"

static int	next_insertion_pos(int n, int **a);

void	push_back(t_group *tg)
{
	pa(tg->b, tg->a, tg->len);
	while (*(tg->b))
	{
		if (**(tg->b) > stack_max(tg->a))
		{
			goto_position(tg->a, get_position(tg->a, stack_min(tg->a)), 'a');
		}
		else
		{
			goto_position(tg->a, next_insertion_pos(**(tg->b), tg->a), 'a');
		}
		pa(tg->b, tg->a, tg->len);
	}
	goto_position(tg->a, get_position(tg->a, stack_min(tg->a)), 'a');
}

static int	next_insertion_pos(int n, int **a)
{
	int	up;
	int	i;

	up = stack_max(a);
	i = -1;
	while (a[++i])
		if (n < *a[i] && *a[i] < up)
			up = *a[i];
	return (get_position(a, up));
}
