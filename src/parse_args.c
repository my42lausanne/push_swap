/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 14:16:53 by dfarhi            #+#    #+#             */
/*   Updated: 2022/01/03 16:42:55 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <limits.h>

int			word_len(char *str);
int			set_level(char *word);
static int	get_word(t_list **lst, char *str);
static int	**ft_lst_intarray(t_list **lst);
static int	ft_atoi_ps(char *word);

int	**parse_args(int ac, char **av, int *len)
{
	int		i;
	int		j;
	int		start;
	t_list	*lst;

	lst = 0;
	i = 0;
	*len = 0;
	while (++i < ac)
	{
		j = -1;
		start = 0;
		while (av[i][++j])
		{
			if (!ft_isspace(av[i][j]) && !start)
			{
				(*len)++;
				start = get_word(&lst, &av[i][j]);
			}
			else if (ft_isspace(av[i][j]))
				start = 0;
		}
	}
	return (ft_lst_intarray(&lst));
}

static int	get_word(t_list **lst, char *str)
{
	t_list	*new;
	char	*word;

	word = ft_calloc(word_len(str) + 1, sizeof(char));
	if (!word)
		ft_error();
	ft_strlcpy(word, str, word_len(str) + 1);
	new = ft_lstnew(word);
	if (!new)
		ft_error();
	if (!*lst)
		*lst = new;
	else
		ft_lstadd_back(lst, new);
	return (1);
}

static int	verify_repetition(int **array)
{
	int	i;
	int	j;

	i = -1;
	while (array[++i])
	{
		j = -1;
		while (++j < i)
			if (*array[i] == *array[j])
				return (1);
	}
	return (0);
}

static int	**ft_lst_intarray(t_list **lst)
{
	int		i;
	int		**array;
	t_list	*needle;

	array = ft_calloc(ft_lstsize(*lst) + 1, sizeof(int *));
	if (!array)
		ft_error();
	needle = *lst;
	i = -1;
	while (needle)
	{
		array[++i] = ft_calloc(1, sizeof(int));
		if (!array[i])
			ft_error();
		*array[i] = ft_atoi_ps(needle->content);
		needle = needle->next;
	}
	ft_lstclear(lst, free);
	if (verify_repetition(array))
		ft_error();
	return (array);
}

static int	ft_atoi_ps(char *word)
{
	unsigned long	n;
	int				negative;
	int				i;
	int				level;

	level = set_level(word);
	n = 0;
	i = -1;
	negative = 0;
	while (word[++i])
	{
		if (!i && word[i] == '-')
			negative = -2;
		else if ((!ft_isdigit(word[i]) && !(!i && word[i] == '+')) || i > 10)
			ft_error();
		else if (word[i] != '+')
		{
			n += (unsigned long)(word[i] - '0') *level;
			level /= 10;
		}
	}
	if (n > ((unsigned long)INT_MIN * -1) || (!negative && n > INT_MAX))
		ft_error();
	return ((negative + 1) * n);
}
