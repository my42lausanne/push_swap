/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 12:33:22 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/14 18:58:42 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef SORT_H
# define SORT_H
# include "grouped_sort.h"

void	sort(int **a, int len);
int		stack_issorted(int **array);
int		stack_size(int **array);
int		a_iscomplete(int **a, int len);
int		stack_max(int **array);
int		stack_min(int **array);
void	sort_small(t_group *tg);

#endif
