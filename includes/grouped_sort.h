/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grouped_sort.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/18 20:55:25 by dfarhi            #+#    #+#             */
/*   Updated: 2022/02/15 15:03:01 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GROUPED_SORT_H
# define GROUPED_SORT_H

typedef struct s_group
{
	int	*sorted_list;
	int	*group_max;
	int	*group_min;
	int	*group_nleft;
	int	groupn;
	int	group_size;
	int	len;
	int	**a;
	int	**b;
}	t_group;

t_group	*group_prepare(int **a, int len);
t_group	*group_create(int **a, int len, int group_size);
void	free_t_group(t_group *tg);
void	grouped_sort(t_group *tg);
void	goto_position(int **a, int pos, char stack);
void	catchup(int gplower, t_group *tg);
void	push_back(t_group *tg);
int		negative_index(int **a, unsigned int index);
int		count_lower_nbrs(int **a, int n);
int		is_ingroup(int n, int gp_n, t_group *tg);
int		get_position(int **a, int n);
int		next_push_pos(int **a, t_group *tg, int gpn);

#endif
