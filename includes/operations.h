/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 16:48:54 by davifah           #+#    #+#             */
/*   Updated: 2021/12/17 12:56:35 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATIONS_H
# define OPERATIONS_H

# include <expanded.h>

int		swap(int **array);
void	sa(int **a);
void	sb(int **b);
void	ss(int **a, int **b);
int		push(int **src, int **dst, int len);
void	pa(int **b, int **a, int len);
void	pb(int **a, int **b, int len);
int		rotate(int **array);
void	rr(int **a, int **b);
void	ra(int **a);
void	rb(int **b);
int		rev_rotate(int **array);
void	rrr(int **a, int **b);
void	rra(int **a);
void	rrb(int **b);

void	check_ss(int **a, int **b);
void	check_rr(int **a, int **b);
void	check_rrr(int **a, int **b);

#endif
