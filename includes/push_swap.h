/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 11:51:31 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/22 11:17:38 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "expanded.h"
# include "libft.h"
# include "operations.h"
# include <stdlib.h>

int		**parse_args(int ac, char **av, int *len);
void	ft_error(void);
void	print_intarray(int **array, int len);
void	print_arrays(int **a, int **b, int len);
void	free_intarray(int **array);

#endif
