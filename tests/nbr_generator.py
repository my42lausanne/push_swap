"""
Generates and prints out a randomized list of numbers, all separated with spaces.
To generate a list of x numbers between 0 and x - 1 numbers, execute with the argument 'x'.
Example: 'python3 nbr_generator x'
To generate a list with numbers from x to y - 1, execute with the argument 'x y'.
Example: 'python3 nbr_generator x y'

This script was created to use with the push_swap program created for 42 School Lausanne
Author: Davi Farhi (gitlab.com/davifarhi)
"""
from random import shuffle
import argparse

parser = argparse.ArgumentParser(description='Generate randomized list')
parser.add_argument('n1', type=int,
        help='If n2 is not defined, sets the total number of numbers to generate')
parser.add_argument('n2', type=int,
        help='If n2 is defined, all numbers between n1(included) and n2(excluded) will be generated',
        nargs='?', default=None)
args = parser.parse_args()

if args.n2 is None:
    nbrs = list(range(args.n1))
else:
    if (args.n1 > args.n2):
        args.n1, args.n2 = args.n2 + 1, args.n1 + 1
    nbrs = list(range(args.n1, args.n2))
shuffle(nbrs)

for i in nbrs:
    print(i, end=" ")
