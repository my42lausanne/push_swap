#!/bin/bash
n=$1
argn=$2
threshold=$3

max=-1
min=999999999
max_arg=0
avg=0
time_avg=0
start_time=0
th_count=0

if [ "$threshold" == "" ] ; then
	threshold=0
fi

if [ "$(basename $(pwd))" == "tests" ] ; then
	exe="../"
	tests=""
else
	exe=""
	tests="tests/"
fi

os=$(uname -s)
if [ "$os" == "Darwin" ] ; then
	checker="./$(echo $tests)checker_Mac"
elif [ "$os" == "Linux" ] ; then
	checker="./$(echo $tests)checker_linux"
fi

make -C $(echo $exe). push_swap

for i in $(seq 1 $n)
do
	time=0
	arg=$(python3 $(echo $tests)nbr_generator.py $argn)

	if [ "$os" != "Darwin" ] ; then
		start_time=$(($(date +%s%N)/1000))
	fi
	op=$(./$(echo $exe)push_swap "$arg")
	if [ "$os" != "Darwin" ] ; then
		time=$(( ($(date +%s%N)/1000) - $start_time ))
	fi

	if [ "$op" == "" ] ; then
		c=0;
	else
		c=$(echo "$op" | wc -l | xargs)
	fi
	res=$(./$(echo $exe)push_swap "$arg" | $checker $arg 2>&1)
	if [ "$res" = "KO" ] || [ "$res" = "Error" ] ; then
		echo -e "operation\n$op\nargs\n$arg\nerror: $res" > .test_arg_$i
		echo "Push_swap KOed by the checker, view ./.test_arg_$i to get the argument list"
	fi

	if [ $c -gt $max ] ; then
		max=$c
		max_arg=$arg
	fi
	if [ $c -lt $min ] ; then
		min=$c
	fi
	if [ $c -ge $threshold ] ; then
		th_count=$(($th_count + 1))
	fi

	avg=$(($avg + $c))
	if [ "$os" != "Darwin" ] ; then
		time_avg=$(($time_avg + $time))
	fi

	echo -ne "$i/$n ($(($i * 100 / $n))%) avg: $(($avg / $i)) time: $(($time))µs  \r"
done

echo
echo -e "max: $max\narg:\n$max_arg\n" > .test_max
echo "$n samples"
echo "$argn numbers"
echo "min $min"
echo "avg $(($avg / $n))"
echo "max $max"
if [ "$os" != "Darwin" ] ; then
	echo "avg time $(($time_avg / $n))µs"
fi
if [ $threshold -ne 0 ] ; then
	echo "count over $threshold: $th_count"
fi
echo "max arguments saved to ./.test_max"
